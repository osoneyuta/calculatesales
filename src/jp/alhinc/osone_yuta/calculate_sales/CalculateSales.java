package jp.alhinc.osone_yuta.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;


public class CalculateSales {


	// 売上ファイルの売上高確認メソッド
	public static boolean saleCheck(String sale, File file) {
		// 2行目がない場合、エラーメッセージを表示して処理終了。
		if (sale == null) {
			System.out.println(file.getName() + "のフォーマットが不正です");
			return false;

		}



		// branchSalesに数字以外の文字が入っている場合、エラーメッセージを表示して処理終了。
		if (sale.matches("^[0-9]*$") != true) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}

		return true;
	}



	// 支店定義ファイルチェックメソッド
	public static boolean codeCheck(String[] codeName, String name) {
		// branchCodeNameの中身が2じゃない場合、エラーメッセージを表示して処理終了。
		if (codeName.length != 2) {
			System.out.println(name + "定義ファイルのフォーマットが不正です");
			return false;
		}


		//支店コードが3桁、中身が数字じゃない場合、エラーメッセージ処理終了。
		if ((codeName[0].matches("^[0-9]{3}") != true)) {
			System.out.println(name + "定義ファイルのフォーマットが不正です");
			return false;
		}

		return true;
	}



	// 連番チェックメソッド
	public static boolean serialNumberCheck(ArrayList<String> files) {

		// i+1番目の連番 - i番目の連番 = 1が求められることで連番ということが証明できる。
		for (int i = 0; i < files.size() -1; i++) {


			File checkFile1 = new File(files.get(i));
			File checkFile2 = new File(files.get(i+1));

			int fileCheck1 = Integer.parseInt(checkFile1.getName().substring(0,8));
			int fileCheck2 = Integer.parseInt(checkFile2.getName().substring(0,8));

			if ((fileCheck2 - fileCheck1) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}


		}

		return true;
	}


	// 売上高桁数確認メソッド
	public static boolean newSaleCheck(HashMap<String,Long> sales, String code) {


		// 売上高をlengthCheckに格納し、桁数を調べる。10桁以上なら処理終了。(branchSales.get(branchCode) + Long.parseLong(branchSale))
		String lengthCheck = String.valueOf(sales.get(code));

		if (lengthCheck.length() > 10) {
			System.out.println("合計金額が10桁を超えました");
			return false;
		}

		return true;
	}


	// 支店定義ファイル読み込みメソッド
	public static boolean input (String[] path, String fileName, HashMap<String, String> names, HashMap<String, Long> sales) {

		// コマンドライン引数が1つじゃない場合、エラーメッセージを表示して処理終了。
		if (path.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}

		BufferedReader br = null;

		try {

			File branchlst = new File(path[0],fileName);

			// branchlstにファイルが存在しない場合、エラーメッセージを表示して処理終了。
			if (branchlst.exists() != true)  {

				System.out.println("支店定義ファイルが存在しません");
				return false;

			}

			FileReader fr = new FileReader(branchlst);
			br = new BufferedReader(fr);

			// branchLstから1行読み込む為に定義.
			String branchlstLine;

			while ((branchlstLine = br.readLine()) != null) {


				String[] branchCodeName = branchlstLine.split(",");


				// 支店コードのエラーメッセージをまとめたメソッドを使用、falseが返ってきたら処理終了。
				if ((codeCheck(branchCodeName, "支店")) != true) {
					return false;
				}

				// ハッシュマップbranchNameに支店コードをキーとして、支店名を要素としてput。
				names.put(branchCodeName[0], branchCodeName[1]);

				// ハッシュマップbranchSalesに支店コードをキーとして、売上高0を要素としてput。
				sales.put(branchCodeName[0], 0L);


			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally {

			try {

				if (br != null) {
					br.close();
				}

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			}
		}

		return true;

	}


	// 支店別集計ファイル出力メソッド
	public static boolean output(String path, String fileName, HashMap<String,String> names, HashMap<String, Long> sales) {
		PrintWriter pw = null;

		try {
			// 出力するファイル名を定義。
			File out = new File(path, fileName);
			FileWriter fw = new FileWriter(out);
			BufferedWriter bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);

			// ハッシュマップmapに入っているキーの数だけ処理を繰り返す。
			for (String keys : names.keySet()) {

				// (支店コード, 支店名, 売上高) と記述する。
				pw.println(keys + "," + names.get(keys) + "," + sales.get(keys));

			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		} finally {
			if (pw != null) {
				pw.close();
			}

		}

		return true;
	}




	public static void main(String[] args) {


		// 各支店のコードと支店名を格納。
		HashMap<String, String> branchName = new HashMap<String, String>();

		// 各支店の売上を格納。
		HashMap<String, Long> branchSales = new HashMap<String, Long>();

		// rcdファイルを格納。
		ArrayList<String> rcdFiles = new ArrayList<String>();


		// 支店定義ファイル読み込みメソッドでfalseが返ってきた場合、処理終了。
		if ((input(args, "branch.lst", branchName,branchSales)) != true ) {
			return;
		}



		// 末尾の拡張子が.rcdのファイルのみtrueを返す。str.lastIndexOf("rcd")
		FilenameFilter rcdFilter = new FilenameFilter() {
			public boolean accept(File file, String str) {

				File checkrcd = new File(file,str);

				if (str.matches("^[0-9]{8}.rcd$") && checkrcd.isFile()) {

					return true;
				} else {
					return false;
				}
			}
		};


		// ディレクトリ内のrcdファイルを配列に格納。
		File[] rcdList = new File(args[0]).listFiles(rcdFilter);


		for (int i = 0; i < rcdList.length; i++) {

			// リストrcdFilesにrcdファイルを格納していく。
			rcdFiles.add(rcdList[i].toString());


		}


		// 連番チェックメソッドで歯抜けになっていないか調べる。
		if ((serialNumberCheck(rcdFiles)) != true) {
			return;
		}


		BufferedReader br = null;

		try {

			for(int i = 0; i < rcdFiles.size(); i++) {
				File rcdFile = new File(rcdFiles.get(i));
				FileReader fr = new FileReader(rcdFile);
				br = new BufferedReader(fr);


				// 支店コードを格納するために定義。
				String branchCode;

				// 売上高を格納するために定義。nullのチェックが出来ないためString型。
				String branchSale;

				// 3行目以降の行があるか確認するために定義。
				String check;


				// 1行目の支店コードを読み込み、branchCodeに格納。
				branchCode = br.readLine();


				// 受け取った支店コードが支店定義ファイルに存在しない支店コードの場合、エラーメッセージを表示して処理終了。
				if (branchName.containsKey(branchCode) != true) {
					System.out.println(rcdFile.getName() +  "の支店コードが不正です");
					return;
				}


				// 2行目の売上高を読み込み、branchSaleに格納。
				branchSale = br.readLine();


				// saleCheckメソッドで2行目のエラーを確認
				if ((saleCheck(branchSale, rcdFile)) != true) {
					return;
				}

				// 3行目を読み込む。
				check = br.readLine();


				// 3行目以降がある場合、エラーメッセージを表示して処理終了。
				if (check != null) {
					System.out.println(rcdFile.getName() + "のフォーマットが不正です");
					return;
				}

				// branchSalesに支店コードをキーとして、前の売上高と受け取った売上高を合算した値を要素としてput。
				branchSales.put(branchCode,(branchSales.get(branchCode)+ Long.parseLong(branchSale)));

				// 売上高が10桁以上の場合エラーメッセージを表示して処理終了。
				if((newSaleCheck(branchSales,branchCode)) != true) {
					return;
				}

			}


		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		} finally {

			try {

				if (br != null) {
					br.close();
				}

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			}
		}

		// 支店別集計ファイル出力メソッドでfalseが返ってきた場合、処理終了。
		if ((output(args[0], "branch.out", branchName, branchSales)) != true ) {
			return;
		}


	}

}


